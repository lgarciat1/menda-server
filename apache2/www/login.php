<?php
#DEV TOOLS. ¡COMENTAR LÍNEA  3 ANTES DE HACER COMMIT!
#ini_set('display_errors', 'On'); // Something useful!
?>

<!DOCTYPE html>
<html lang="es-ES" dir="ltr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./sprint7.css" />
    <title>Menda</title>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="./script.js"></script>
</head>

<body>
    <div class="logo" style="margin-bottom: 40px; margin-top: 40px;">
        <img src="static/menda.png" width="110vw" height="auto" alt="mascotasplus logo">
    </div>
    <div class="wrapper">
        <div class="title-text">
            <div class="title login">Iniciar Sesión</div>
        </div>
		<!--Errores que se mostraran en caso de que se produzcan-->
		<?php
		//Error que se devuelve cuando no hay ningún usuario con el mail introducido
		if (isset($_GET['login_failed_email']) == 'True') {
			echo '<p id="error-log">No existe ningún usuario registrado con ese email.</p>';
		}

		//Error que se devuelve cuando la contraseña no es correcta
		if (isset($_GET['login_failed_password']) == 'True') {
			echo '<p id="error-log">La contraseña no es correcta.</p>';
		}

		//Error que se devuelve cuando se desconoce qué ha causado el error
		if (isset($_GET['login_failed_unknown']) == 'True') {
			echo '<p id="error-log">Error desconozido. Si el error persiste, inténtalo más tarde.</p>';
		}
		//Nota: Las sentencias echo para imprimir líneas HTML deben ir entre comillas simples, '', para evitar errores
		//a la hora de añadir id's u otros atributos.
		?>
        <div class="form-container">
            <div class="form-inner">
                <form action="do_login.php" method="post" class="login">
                    <div class="field">
                        <input name="email" id="email" type="text" placeholder="Correo electrónico" required />
                    </div>
                    <div class="field">
                        <input name="password" id="password" type="password" placeholder="Contraseña" required />
                    </div>
                    <div class="pass-link">
                        <a href="#">¿Olvidaste tu contraseña?</a>
                    </div>
                    <div class="field btn">
                        <div class="btn-layer"></div>
                        <input type="submit" value="Iniciar sesión" />
                    </div>
                    <div class="signup-link">
                        ¿No eres miembro? <a href="register.php">Registrate</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>