<?php
ini_set('display_errors', 'On');
require __DIR__ . '/../php_util/db_connection.php';

session_start();
$mysqli = get_db_connection_or_die();
// si la variable $_SESSION['user_id] esta vacía redirigirá al login
if (empty($_SESSION['user_id'])) {
    header('Location: login.php');
}

if (isset($_GET['failed'])) {
    if($_GET['failed'] == True){
        die("La creación de la obra ha fallado");
     }
}
//Hacemos una consulta seleccionando profile_type para luego comprobar si es de tipo provider o builder
$query = "SELECT profile_type FROM tUser WHERE id = " .$_SESSION['user_id'];
$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
$row = mysqli_fetch_array($result);
?>
<!-- Si es de tipo builder mostrará el formulario . -->
<?php if ($row['profile_type'] != "builder"){
    die("Los providers no añaden obras");
 } ?> 
    <!DOCTYPE html>
<html lang="es-ES">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet" />
    <link href="./static/estilos.css" rel="stylesheet" type="text/css" />
    <title>Creación de las obras:</title>
</head>

<body>
    <div class="header">
        <h2 class="blanco">MENDA</h2>
    </div>
   
        <div id="contenedor">
            <div class="center">
                <h1 class="blanco">Crear obra</h1>
                <form action="do_create_construction.php" name="myForm" method="post" id="f_create_contruction">
                    <ul>
                        <li>
                            <label class="titulo" for="nombre">Datos obras:</label>

                            <div class="controles">
                                <span class="completo">
                                    <input type="text" name="f_building_name" id="build_name" required />
                                    <label for="f_building_name">Nombre de la obra:</label>
                                </span>

                                <span class="completo">
                                    <input type="text" name="f_architect" id="architect_name" required />
                                    <label for="f_architect">Nombre del arquitecto:</label>
                                </span>

                                <span class="completo">
                                    <input type="text" name="f_hirer" id="hirer_name" required />
                                    <label for="f_hirer">Nombre del arrendador:</label>
                                </span>
                            </div>
                        </li>
                        <li>
                            <label class="titulo" for="telefonofijo">Fechas:</label>

                            <div class="controles">
                                <span class="fechas">
                                    <input type="date" name="f_start_date" id="start_date" required />
                                    <label for="f_start_date">Fecha inicio:</label>
                                </span>

                                <span class="fechas">
                                    <input type="date" name="f_end_date" id="end_date" required class="margen" />
                                    <label for="f_end_date" class="margen">Fecha fin:</label>
                                </span>
                            </div>
                        </li>
                        <li>
                            <label class="titulo" for="direccion">Dirección:</label>

                            <div class="controles">
                                <span class="completo">
                                    <input type="text" name="f_address" id="address_name" required />
                                    <label for="f_address">Introduce la direccion:</label>
                                </span>

                                <span class="latitude">
                                    <input type="text" name="f_latitude" id="latitude_name" placeholder="ej:48.124332" required />
                                    <label for="f_latitude">Introduce la latitud:</label>
                                </span>

                                <span class="longitude">
                                    <input type="text" name="f_longitude" id="longitude_name" placeholder="ej:11.401987"required class="margen" />
                                    <label for="f_longitude" class="margen">Introduce la longitud:</label>
                                </span>
                            </div>
                        </li>
                        <li class="botones">
                            <button type="submit" class="pulse-button" value="Ejecutar">
                                <img src="./static/enviar.png" alt="enviar" width="50px" height="auto" />
                            </button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <div class="footer">
            <p class="blanco">© Menda</p>
        </div>
    
</body>

</html>
   


   


 

