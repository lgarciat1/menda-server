<!DOCTYPE html>
<html lang="es_ES">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./sprint7.css" />
    <title>Confirmación de materiales</title>
</head>

<body>

    <?php
    // ini_set('display_errors', 'on');

    require __DIR__ . '/../php_util/db_connection.php';
    $mysqli = get_db_connection_or_die();
    session_start();

    if (mysqli_query(
        $mysqli,
        "UPDATE tOrder inner join tOrder on tOrder.id = tOrderItem.order_id inner join tConstruction on tOrder.construction_id = tConstruction.id SET tOrder.status = 'confirmed' WHERE tConstruction.author_id = " . $_session['user_id'] . " and tOrder.id = " . $_get['order_id']
       
    
        ) === TRUE) {
        echo "<p>Orden confirmada</p>";
    } else {
        echo "<p>Orden denegada</p>";
    }
    ?>
</body>

</html>