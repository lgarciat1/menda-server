<?php
    //Permite la conexión a la base de datos
    ini_set ('display_errors', 'On');
    require __DIR__ . '/../php_util/db_connection.php';
    $mysqli = get_db_connection_or_die();

    session_start();
    //Si la variable 'user_id' se encuentra vacía muestra una página de error 404 
    $_SESSION['user_id'];

    if (empty($_SESSION['user_id'])){
        http_response_code(404);
        include('404_login.html');
        die();
    }

    //Si no existe la variable 'id' muestra una página de error 404
    if(!isset($_GET['id'])){
        http_response_code(404);
        include('404_construction.html');
        die();
    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <!-- Estilos CSS del sitio -->
        <style>
            table, td, tr, th{
                border-collapse: collapse;
                border: 1px solid black;
                text-align: center;
                padding: 5px;
                background-color: lightgrey;
                opacity: 0.95;
            }
            div.header{
                background-color: black;
                color: lightgrey;
            }
            div.header button{
                position: absolute;
                left:87%;
                top: 1.5%;
            }
            div.footer{
                background-color: black;
                color: lightgrey;
                position: absolute;
                bottom:0%;
                width:100%;
            }
            body{
                background-color: lightgrey;
                background-image: url(/static/excavadora.jpg);
                background-repeat: no-repeat;
                background-size: cover;
                font-family: 'Roboto', sans-serif;
            }
            *{
                margin: 0;
                box-sizing: border-box;
            }
            th{
                background-color: brown;
            }
            table{
                margin: auto;
            }
            a:hover{
                font-weight:bold;
            }
            button:hover{
                font-weight:bold;
            }
            button{
                background-color: lightgrey;
                border-radius: 5px;
            }
            div.botones{
                position:absolute;
                left: 22%;
            }
        </style>
    </head>
    <body>
        <!-- Encabezado -->
        <div class="header">
            <h1 style="margin-left: 1%;">Menda</h1>
            <!-- Boton que permite hacer logout al usuario que lo pulse -->
            <button><a style="text-decoration: none; color:black; padding: 5px" href="/do_logout.php">Logout</a></button> 
        </div>
        <!-- Título -->
        <h1 style="text-align: center; padding:15px; margin-bottom: 1%; color:lightyellow">Pedidos</h1>
        <?php
            // Recoge el id de la cabecera y lo muestra en el sitio web
            $idobra= $_GET['id'];
            echo '<h3 style="text-align:center; margin-bottom: 2%; color:lightyellow">ID de obra: '.$idobra. '</h3>'
        ?>
        <table>
            <tr>
                <th>ID de pedido</th>
                <th>Nombre empresa proveedora</th>
                <th>Estado</th>
                <th>Fecha de creación</th>
                <th>Página de pedido</th>
            </tr>
        <?php
            // Realiza una consulta a la BBDD para acceder a los pedidos correspondientes a ID de obra
            $consulta = 'SELECT tOrder.id, tUser.business_name, tOrder.status, tOrder.date_created FROM tUser JOIN tOrder ON tUser.id=tOrder.provider_id 
            WHERE tOrder.construction_id='.$idobra.' ORDER BY tOrder.id';
            $resultado = mysqli_query($mysqli, $consulta) or die('Query Error');
            while($fila = mysqli_fetch_array($resultado)){
                echo '<tr>';
                echo '<td>'.$fila['id'].'</td>';
                echo '<td>'.$fila['business_name'].'</td>';
                echo '<td>'.$fila['status'].'</td>';
                echo '<td>'.$fila['date_created'].'</td>';
                echo '<td><a href="/order.php/?id='.$fila['id'].'">Pedido</a></td>'; 
                echo '</tr>';
            }
        ?>
        </table>
        <?php
            // Realiza una consulta a la BBDD para mostrar el botón 'Registrar nuevo pedido' solo a los usuarios tipo 'builder'
            $user_id = $_SESSION['user_id'];
            $consultabuilder = 'SELECT profile_type FROM tUser WHERE id='.$user_id.'';
            $resultadobuilder = mysqli_query($mysqli, $consultabuilder) or die('Query Error');
            while($row = mysqli_fetch_array($resultadobuilder)){
                if($row['profile_type'] == 'builder'){
                    echo '<br>';
                    echo '<div class="botones">';
                    //Update: se manda el id de la obra para poder hacer nuevos pedidos a esa misma obra
                    echo '<button><a style="text-decoration: none; color:black; padding: 5px;" href="/create_order.php/?id='.$idobra.'">Registrar nuevo pedido</a></button>';
                    echo '</div>';
                }
            }
            mysqli_close($mysqli); //Cierra la conexión con la BBDD
        ?>
        <!-- Pie de página -->
        <div class="footer"> 
            <p style="margin-left:1%">© 2022 Menda</p>
        </div>
    </body>
</html>