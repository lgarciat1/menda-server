<?php
	ini_set('display_errors', 'On');
	require __DIR__ . '/../php_util/db_connection.php';
	session_start();
	$mysqli = get_db_connection_or_die();
    $user_id = $_SESSION['user_id'];
?>
<!DOCTYPE html>
	<head>
		<meta charset="UTF8">
		<title>HOME</title>
		<!--link estilos css-->
        <link rel="stylesheet" href="./static/estilos_main.css"/>
		<style>
			.superior{
				height: 200px;
				width: 200px;
				background-color: #f2f2f2;
				border-radius: 20%;
				margin: auto;
				margin-top: 10px;
				padding: 20px;
			}
		</style>
	</head>
	<body>
		<!--Contenido PHP-->
		<div class="container3">
			<!--Boton logout-->
			<div class="boton_logout">
				<button onclick="window.location.href='/do_logout.php'">Logout</button>
			</div>
			<?php
				#Comprobamos que la sesión no está vacía
				if(empty($user_id)) {
					header('Location: 404_main.html');
				}else{
					#Instanciamos una variable donde cogemos todos los datos del usuario que tenga como id el comprobado anteriormente
					$profile_type = 'SELECT * FROM tUser WHERE id='.$user_id;
					$result = mysqli_query($mysqli, $profile_type) or die('Query Error');
					#Almacenamos en la variable result la query
					$row1 = mysqli_fetch_array($result);
					#Comprobamos si la columna profile_type es Constructores o Proveedores
					switch ($row1['profile_type']) {
						case "builder":
							echo '<h1 class="titulo">Constructor</h1>';
							$query = 'SELECT * FROM tConstruction WHERE author_id ='.$user_id;
							$result1 = mysqli_query($mysqli, $query) or die('Query Error');
							echo '<div class="contenedor">';
							echo '<table class="default">';
							echo '<tr>';
							echo '<th>OBRA</th>';
							echo '<th>ARQUITECTO</th>';
							echo '<th>ARRENDADOR</th>';
							echo '<th>FECHA DE INICIO</th>';
							echo '<th>FECHA DE FIN</th>';
							echo '<th>DIRECCION</th>';
							echo '<th>LATITUD</th>';
							echo '<th>LONGITUD</th>';
							echo '<th>ENLACE</th>';
							echo '</tr>';
							#Recorremos $result1, almacenando los datos en un array
							while ($row = mysqli_fetch_array($result1)) {
								echo '<tr>';
								#Mostramos los datos que queremos
								echo '<td>'.$row['building_name'].'</td>';
								echo '<td>'.$row['architect'].'</td>';
								echo '<td>'.$row['hirer'].'</td>';
								echo '<td>'.$row['start_date'].'</td>';
								echo '<td>'.$row['end_date'].'</td>';
								echo '<td>'.$row['address'].'</td>';
								echo '<td>'.$row['latitude'].'</td>';
								echo '<td>'.$row['longitude'].'</td>';
								echo '<td><a href="http://localhost:8089/construction.php?id='.$row['id'].'">Obra Nº '.$row['id'].'</td>';
								echo '</tr>';
							}
							echo '</table>';
							echo '</div>';
							?>
							<div class="superior">
								<p>¿Quieres añadir un nuevo evento?</p>
								<div class="botones1">
									<button class="evento" onclick="window.location.href='/create_construction.php'">Crear Evento</button>
								</div>
							</div>
							<?php
							break;
						case "provider":
							echo '<h1 class="titulo">Provedores</h1>';
							#Realizamos un Join para relacionar las tres tablas tOrder, tConstruction y tUser  y cogemos los datos que necesitamos
							$query = 'SELECT  tConstruction.building_name, tConstruction.architect, tConstruction.hirer, tConstruction.start_date, tConstruction.end_date, tConstruction.address, tConstruction.latitude, tConstruction.longitude  FROM tConstruction JOIN tOrder ON  tOrder.construction_id = tConstruction.id WHERE tOrder.provider_id =' .$user_id ;
							$result1 = mysqli_query($mysqli, $query) or die('Query Error');
							echo '<div class="contenedor">';
							echo '<table class="default">';
							echo '<tr>';
							echo '<th>OBRA</th>';
							echo '<th>ARQUITECTO</th>';
							echo '<th>ARRENDADOR</th>';
							echo '<th>FECHA DE INICIO</th>';
							echo '<th>FECHA DE FIN</h1>';
							echo '<th>DIRECCION</h1>';
							echo '<th>LATITUD</h1>';
							echo '<th>LONGITUD</h1>';
							echo '</tr>';
							#Recorremos $result1, almacenando los datos en un array
							while ($row = mysqli_fetch_array($result1)) {
								echo '<tr>';
								#Mostramos los datos que queremos
								echo '<td>'.$row['building_name'].'</td>';
								echo '<td>'.$row['architect'].'</td>';
								echo '<td>'.$row['hirer'].'</td>';
								echo '<td>'.$row['start_date'].'</td>';
								echo '<td>'.$row['end_date'].'</td>';
								echo '<td>'.$row['address'].'</td>';
								echo '<td>'.$row['latitude'].'</td>';
								echo '<td>'.$row['longitude'].'</td>';
								echo '</tr>';
							}
							echo '</table>';
							echo '</div>';
							break;
						default:
							header('Location: error_main.html');
							break;
					}
					#Cerramos la conexión a la base de datos
					mysqli_close($mysqli);
				}
			?>
		</div>
	</body>
</html>
