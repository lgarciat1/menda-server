<!DOCTYPE html>
<html lang="es_ES">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./sprint7.css" />
    <title>Confirmación de materiales</title>
</head>

<body>

    <?php
    // ini_set('display_errors', 'on');

    require __DIR__ . '/../php_util/db_connection.php';
    $mysqli = get_db_connection_or_die();
    session_start();
    $result = mysqli_query(
        $mysqli,
        "SELECT * FROM tOrderItem inner join tOrder on tOrder.id = tOrderItem.order_id inner join tConstruction on tOrder.construction_id = tConstruction.id where tConstruction.author_id = " . $_SESSION['user_id'] . " and tOrder.id = " . $_get['order_id']
    );
    // $result = mysqli_query(
    //     $mysqli,
    //     'SELECT * FROM tOrderItem inner join tOrder on tOrder.id = tOrderItem.order_id inner join tConstruction on tOrder.construction_id = tConstruction.id where tConstruction.author_id = 24 and tOrder.id = 11'
    // );

    // Creo estas variables y las comparo posteriormente para saber si han funcionado todos los UPDATE
    $i = 0;
    $i2 = 0;
    while ($row = mysqli_fetch_array($result)) {
        $i2++;
        // Compruebo si el campo es un Int
        if (is_int($_REQUEST[$row['item_name']])) {
            // Si el UPDATE es correcto, incremento la variable $i
            if (mysqli_query(
                $mysqli,
                "UPDATE tOrderItem inner join tOrder on tOrder.id = tOrderItem.order_id inner join tConstruction on tOrder.construction_id = tConstruction.id  SET tOrderItem.accepted_quantity = " . $_REQUEST[$row['item_name']] . " WHERE tConstruction.author_id = " . $_SESSION['user_id'] . " and tOrderItem.item_name = '" . $row['item_name'] . "'"
            ) === TRUE) {
                $i++;
            }
        }
    }
    if ($i === $i2) {
        echo '<h1>Materiales confirmados</h1>';
    } else {
        echo '<h1>Error al confirmar los materiales</h1>';
    }
    ?>
</body>

</html>