<!DOCTYPE html>
<html lang="es_ES">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./sprint7.css" />
    <title>Confirmación de materiales</title>
</head>

<body>
    <?php
    // ini_set('display_errors', 'On');

    require __DIR__ . '/../php_util/db_connection.php';
    $mysqli = get_db_connection_or_die();
    session_start();
    $result = mysqli_query(
        $mysqli,
        "SELECT * FROM tOrderItem inner join tOrder on tOrder.id = tOrderItem.order_id inner join tConstruction on tOrder.construction_id = tConstruction.id where tConstruction.author_id = " . $_SESSION['user_id'] . " and tOrder.id = " . $_GET['order_id']
    );

    // $result = mysqli_query(
    //     $mysqli,
    //     'SELECT * FROM tOrderItem inner join tOrder on tOrder.id = tOrderItem.order_id inner join tConstruction on tOrder.construction_id = tConstruction.id where tConstruction.author_id = 24 and tOrder.id = 11'
    // );
    $i = 0;
    echo '<h1>Materiales del pedido</h1>';
    echo '<form action="/do_order_materials.php?order_id=' . $_GET['order_id'] . '" method="post">';
    // Muestro los materiales del pedido
    while ($row = mysqli_fetch_array($result)) {
        $i++;
        echo '<p>' . $row['item_name'] . '</p>';
        echo '<input type=number name=' . $row['item_name'] .
            ' placeholder=' . $row['quantity'] . ' aria-label=' . $row['item_name'] . ' autocomplete=' . $row['item_name'] . ' required>';
    }
    // Si no se muestra ningun material no se muestra el boton
    if ($i !== 0) {
        echo '<br><br><br>';
        echo '<button type="submit">Confirmar</button>';
        echo '</form>';
    }
    ?>
</body>

</html>