from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Tuser, Tconstruction, Torder, Torderitem
from django.db import IntegrityError
from django.utils import timezone
import json

@csrf_exempt
def prueba(request,id):
    try:
        token= request.headers.get('token')
        if not token:
            return JsonResponse(status=401, data={401:'La petición ha fallado porque no se ha enviado el token de autenticación en las cabeceras'})

        usuario = Tuser.objects.get(active_session_token=token)
        construccion = Tconstruction.objects.get(id=id)
        if construccion.author.id != usuario.id:
            return JsonResponse(status=403, data={403:'El token de sesión no se corresponde con el usuario autor de la obra'})
        body_data={}
        body_data=json.loads(request.body)
        order=Torder(
	    date_created=timezone.now(),
            construction_id=id,
            provider_id=body_data["provider_id"],
            status="pending_pricing"
        )
        order.save()
        for items in body_data["order_items"]:
            item=Torderitem(
                order_id=order.id,
                item_name=items["item_name"],
                quantity=items["quantity"],
                measurement_unit=items["measurement_unit"],
            )
            item.save()
        return JsonResponse(status=201, data={201: 'Ha funcionado'})
    except ValueError or KeyError:
        return JsonResponse(status=400, data={400:'La petición ha fallado porque falta algún parámetro o no es del tipo correcto'})
    except IntegrityError or Tconstruction.DoesNotExist:
        return JsonResponse(status=404, data={404:'La petición ha fallado porque no existe una obra con el ID especificado o porque no existe un Proveedor con el ID especificado en provider_id'})
    except Tuser.DoesNotExist:
        return JsonResponse(status=403, data={403:'La petición ha fallado porque el token de sesión no es válido'})

