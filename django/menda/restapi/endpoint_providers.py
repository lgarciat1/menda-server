from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from .models import Tuser

@csrf_exempt

def providers_query_result(request):
	#Guardamos la string de busqueda de la app
	q = request.GET.get('q')
	#Si viene vacía devolvemos un error 400
	if not q:
		return  JsonResponse(status=400, data={})
	result = []
	#En caso de no venir vacía devolvemos los resultados que cumplan con la búsqueda
	matching_users = Tuser.objects.filter(business_name__icontains=q)
	for  matching_user in matching_users:
		result.append({"provider_id": matching_user.id ,"business_name": matching_user.business_name})
	return JsonResponse(status=200, data=result,safe=False)
