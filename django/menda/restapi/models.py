# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Tconstruction(models.Model):
    building_name = models.CharField(max_length=100)
    architect = models.CharField(max_length=100)
    hirer = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField()
    address = models.CharField(max_length=200)
    latitude = models.DecimalField(max_digits=8, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    author = models.ForeignKey('Tuser', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'tConstruction'


class Torder(models.Model):
    status = models.CharField(max_length=20)
    date_created = models.DateField()
    construction = models.ForeignKey(Tconstruction, models.DO_NOTHING)
    provider = models.ForeignKey('Tuser', models.DO_NOTHING)
    delivery_builder_signature = models.TextField(blank=True, null=True)
    delivery_carrier_signature = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tOrder'


class Torderitem(models.Model):
    order = models.ForeignKey(Torder, models.DO_NOTHING)
    item_name = models.CharField(max_length=200)
    quantity = models.IntegerField()
    measurement_unit = models.CharField(max_length=2)
    price = models.IntegerField(blank=True, null=True)
    accepted_quantity = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tOrderItem'


class Tuser(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=100)
    email = models.CharField(unique=True, max_length=200)
    business_name = models.CharField(max_length=100)
    business_phone_country_code = models.CharField(max_length=4)
    business_phone_number = models.CharField(max_length=11)
    profile_type = models.CharField(max_length=20)
    encrypted_password = models.CharField(max_length=100)
    active_session_token = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tUser'
