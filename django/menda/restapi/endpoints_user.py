from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from .models import Tuser
import random, string, bcrypt

#Función para registrar a nuevos usuarios y controlar errores
@csrf_exempt
def register(request):
  json_body = json.loads(request.body)
  business_info = json_body['business_info']
  business_phone = business_info['phone']
  user_info = json_body['user_info']
  new_user = Tuser()
  new_user.name = user_info['name']
  new_user.surname = user_info['surname']
  new_user.email = user_info['email']
  new_user.encrypted_password = user_info['password']
  new_user.business_name = business_info['name']
  new_user.business_phone_country_code = business_phone['country_code']
  new_user.business_phone_number = business_phone['number']
  new_user.profile_type = json_body['profile_type']

  if new_user.name == "" or new_user.surname == "" or new_user.email == "" or new_user.encrypted_password == "":
   #Código 400: Alguno de los parámetros falta o no es válido
   return JsonResponse(status=400, data={}, safe=False)

  if new_user.business_name == "" or new_user.business_phone_country_code == "" or new_user.business_phone_number == "" or new_user.profile_type == "":
   #Código 400: Alguno de los parámetros falta o no es válido
   return JsonResponse(status=400, data={}, safe=False)

  #Iteración de datos en tUser para ver si se repiten los valores
  tabla_user = Tuser.objects.all()
  for fila_user in tabla_user:
   if new_user.email == fila_user.email:
    #Código 409: Email o empresa con el mismo número repetido
    return JsonResponse(status=409, data={"email": new_user.email}, safe=False)
   elif new_user.business_phone_country_code == fila_user.business_phone_country_code and new_user.business_phone_number == fila_user.business_phone_number:
    #Código 409: Email o empresa con el mismo número repetido
    return JsonResponse(status=409, data={}, safe=False)

  #Encriptar la contraseña
  salt = bcrypt.gensalt()
  hashed = bcrypt.hashpw(new_user.encrypted_password.encode('utf-8'), salt).decode("utf-8")

  new_user.encrypted_password = hashed
  new_user.save()
  #Código 201: Usuario creado con éxito
  return JsonResponse(status=201, data={"name":new_user.name, "surname":new_user.surname, "email":new_user.email, "encrypted_password":new_user.encrypted_password, "business_name": new_user.business_name, "business_phone_country_code":new_user.business_phone_country_code, "business_phone_number": new_user.business_phone_number,"profile_type": new_user.profile_type}, safe=False)

#Función para crear un token aleatorio
def aleatoryToken():
	length_of_string = 20
	return (''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length_of_string)))

#Función para loguear a usuarios y controlar errores de sesión
@csrf_exempt
def login(request):
  json_body = json.loads(request.body)
  email_p = json_body["email"]
  password_p = json_body["password"]

  if email_p == "" or password_p == "":
   #Código 400: Faltan parámetros del login
   return JsonResponse(status=400, data={})

  try:
    fila_usuario = Tuser.objects.get(email=email_p)
    password_encrypted = fila_usuario.encrypted_password
    if not bcrypt.checkpw(password_p.encode("utf-8"), password_encrypted.encode("utf-8")):
     #Código 401: La contraseña introducida es incorrecta
     return JsonResponse(status=401, data={"password_encrypted":password_p})
  except Tuser.DoesNotExist:
    #Código 404: El usuario logueado no existe
    return JsonResponse(status=404, data={"email":email_p})

  #Asignar el token aleatorio a la variable para guardar el token en la tabla
  fila_usuario.active_session_token = aleatoryToken()
  fila_usuario.save()

 #Código 201: usuario logueado con éxito 
  return JsonResponse(status=201, data={'user_id': fila_usuario.id, 'name': fila_usuario.name, 'profile_type': fila_usuario.profile_type, 'token': fila_usuario.active_session_token})



