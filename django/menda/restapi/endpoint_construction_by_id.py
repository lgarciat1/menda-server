from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from .models import Tconstruction, Torder, Torderitem, Tuser

def devolverConstruccion(request,idCons):
	
	if request.method != 'GET':
		return JsonResponse(status=405, data={})
	#No Existe Token (ERROR 401)
	try:
		token = request.headers["token"]
	except KeyError:
		return JsonResponse(status=401, data={})
	#Token Invalido(ERROR 403)
	try:
		filaUsuario = Tuser.objects.get(active_session_token=token)
	except Tuser.DoesNotExist:
		return JsonResponse(status=403, data={})	
	#No Existe Contruccion (ERROR 404) 
	try:
		filaConstruction = Tconstruction.objects.get(id=idCons)
	except Tconstruction.DoesNotExist:
		return JsonResponse(status=404, data={})
	#Existe Construccion(202)
	pedidos = filaConstruction.torder_set.all()
	diccionario = {}
	diccionario['building_name'] = filaConstruction.building_name
	diccionario['architect'] = filaConstruction.architect
	diccionario['hirer'] = filaConstruction.hirer
	diccionario['start_date'] = filaConstruction.start_date
	diccionario['end_date'] = filaConstruction.end_date
	diccionario['location'] = {
	'address':filaConstruction.address,
	'latittude':filaConstruction.latitude,
	'longitude':filaConstruction.longitude
	}
	diccionario['orders'] = []
	for fila in pedidos:
		orderItems = fila.torderitem_set.all()
		order = {}
		order['order_id'] = fila.id
		order['status'] = fila.status
		order['provider_business_name'] = fila.provider.business_name
		order['provider_user_email'] = fila.provider.email
		order['order_item'] = []
		for filaI in orderItems:
			if(fila.status == 'pending_pricing'):
				orderi = {}
				orderi['item_name'] = filaI.item_name
				orderi['quantity'] = filaI.quantity
				orderi['measurement_unit'] = filaI.measurement_unit
			elif(fila.status == 'pending_confirmation'):
				orderi = {}
				orderi['item_name'] = filaI.item_name
				orderi['quantity'] = filaI.quantity
				orderi['measurement_unit'] = filaI.measurement_unit
				orderi['price'] = filaI.price
			elif(fila.status == 'confirmed'):
				orderi = {}
				orderi['item_name'] = filaI.item_name
				orderi['quantity'] = filaI.quantity
				orderi['price'] = filaI.price
				orderi['measurement_unit'] = filaI.measurement_unit
			elif(fila.status == 'delivered'):
				orderi = {}
				orderi['item_name'] = filaI.item_name
				orderi['quantity'] = filaI.quantity
				orderi['measurement_unit'] = filaI.measurement_unit
				orderi['price'] = filaI.price
				orderi['accepted_quantity'] = filaI.accepted_quantity
			else:
				return JsonResponse(status=500, data={"msg": "Wrong status" + fila.status})
			order['order_item'].append(orderi)
		diccionario['orders'].append(order)
	return JsonResponse(status=200, data=diccionario)


