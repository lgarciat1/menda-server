from re import S
from django.http  import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from .models import Torder,Tuser,Tconstruction,Torderitem

@csrf_exempt
def quote(request,id,order_id):

    # recojo el valon del token de identificaion
    # con este valor hago comprobaciones en la bbdd
    mi_token = request.headers['token']
    if (mi_token ==""):
        return JsonResponse(status=401, data={'error 401':' no se ha enviado el token de autenticación en las cabeceras'})

   #comprobar que el token existe en Tuser
    try:
        mi_user = Tuser.objects.get(active_session_token = mi_token)
    except:
        return JsonResponse(status=400, data={'error 400':' token no encontrado en la tabla usuario'})
    pos_value_price = 0
    value_price = []
    json_body = json.loads(request.body)
    for items in json_body:
        if (items['op'] == 'replace'):
            value_status = items['value']
            if (value_status == "confirmed"):
                #es la confirmación de un constructor
                #solo se debe cambiar el valor de status de la tabla TOrder a "confirmed"

                # comprobar antes que se trata de un constructor Tuser.profile_type ="constructor"
                if mi_user.profile_type == "builder":
                     #comprobar en la tabla Tconstruction si la fila seleccionada contiene como author.id el mi_user.id
                     # y que el Torder.status = "pending_confirmation"
                    try:
                        mi_construction = Tconstruction.objects.get(id = id)
                        if (mi_construction.author.id == mi_user.id):
                            #comprobar que el provider coincide con el Tuser.id de la sesion
                            # si todo es correcto se puede hacer la actualizacion
                            try:
                                mi_order = Torder.objects.get(id = order_id)
                                if (mi_order.status == "pending_confirmation"):
                                    mi_order.status = value_status
                                    mi_order.save()
                                    return JsonResponse(status =200, data={'codigo 200':'Confirmacion del constructor actualizada con exito, tiene que enviar por email la orden de compra y el plan de entrega a su proveedor'})
                                else:
                                    return JsonResponse(status=409, data={'error=409':'status no se corresponde con pending_confirmation para el constructor'})
                            except:
                                return JsonResponse(status =404, data={'error: 404':'no existe el pedido con el order_id especificado '})
                        else:
                            return JsonResponse(status=403, data={'error=403':'la sesión no ha sido iniciada por el constructor autor de la obra'})
                    except:
                        return JsonResponse(status=404, data={'error=404':'no existe una obra con el id especificado'})
                else:
                    return JsonResponse(status=403, data={'error=403':'el usuario debe ser un constructor'})

            elif (value_status == "pending_confirmation"):
                # comprobar antes que se trata de un proveedor Tuser.profile_type ="provider"
                if mi_user.profile_type == "provider":
                     #comprobar en la tabla Toder si la fila seleccionada contiene como provider_id el mi_user.id
                     # y que el Torder.status = "pending_confirmation"
                    try:
                        mi_order = Torder.objects.get(id = order_id)
                        print(mi_order.status)
                        print(mi_order.provider.id)
                        print(mi_user.id)
                        if (mi_order.status == "pending_pricing") and (mi_order.provider.id == mi_user.id):
                            #comprobar que el provider coincide con el Tuser.id de la sesion

                            # si todo es correcto se puede hacer la actualizacion
                            mi_order.status = value_status
                            mi_order.save()
                            try:
                                order_items = Torderitem.objects.filter(order = mi_order)
                                # otra forma de hacerlo
                                # order_items = order.torderitem_set.all()

                                # en el caso de que devuelva una lista de items
                                # se recorre la lista para actualizar cada uno de los campos value
                                # con value_price[]
                            except Torderitem.DoesNotExist:
                                return JsonResponse(status =400, data={'error: 400':'no existen lineas de pedido en este pedido'})
                        else:
                            return JsonResponse(status=409, data={'error: 409':'status o sesion no se corresponden con pending_pricing o proveedor'})
                    except:
                         return JsonResponse(status =404, data={'error: 404':'no existe el pedido con el order_id especificado '})
                else:
                    return JsonResponse(status=403, data={'error=403':'el usuario que ha iniciado sesión no es un proveedor'})
            else:
                return JsonResponse(status=400, data={'error=400':'parametro value de la op:replace incorrecto'})
        elif (items['op'] == 'add'):
            value_price.append(items['value'])
            # la variable pos_value_price contiene informacion de cuantos items con la op = add hay
            pos_value_price = pos_value_price + 1 
        else:
            return JsonResponse(status=400, data={'error: 400':'Valor erroneo en el parametro op del json'})

    # actualizar el valor de cada uno de los campos price en Torderitem
    # con los valores guardados en value_price[]
    # order_items -- lista con todos las filas de TOrderItem para el order_id dado
    pos_items_order = 0
    if (value_status == "pending_confirmation") and (pos_value_price > 0):
        # si en el json el status es pending_confirmation, debe haber una operación 'add' con un nuevo precio por cada elemento de los order_items
        # si hay valores en order_items entra en el bucle y actualiza los datos de price
        for items_order in order_items:
            if pos_items_order >= pos_value_price:
                return JsonResponse(status=409, data={'error: 409':'El pedido está en pending_pricing, pero no se ha enviado una operación add con un nuevo precio por cada elemento de los order_items'})
            items_order.price = value_price[pos_items_order]
            items_order.save()
            pos_items_order = pos_items_order + 1
    else:
        return JsonResponse(status=409, data={'error: 409':'El pedido está en pending_pricing, pero no se ha enviado ninguna operación add con un nuevo precio'}) 

    # si llega aqui todo bien devuelve 200
    return JsonResponse(status=200, data={'codigo 200':'Cotizacion del proveedor realizada con exito'})
