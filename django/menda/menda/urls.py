"""menda URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from restapi import endpoints_user, endpoints_constructions, endpoint_construction_by_id, endpoint_order_creation, endpoint_order_update
from restapi import endpoint_providers
from restapi import endpoint_delivery

urlpatterns = [
    path('admin/', admin.site.urls),
    path('rest/version/1/sessions', endpoints_user.login),
    path('rest/version/1/users', endpoints_user.register),
    path('rest/version/1/constructions', endpoints_constructions.get_constructions),
    path('rest/version/1/constructions/<int:idCons>', endpoint_construction_by_id.devolverConstruccion),
    path('rest/version/1/constructions/<id>/orders', endpoint_order_creation.prueba),
    path("rest/version/1/constructions/<int:id>/orders/<int:order_id>",endpoint_order_update.quote),
    path('rest/version/1/constructions/<int:id>/orders/<int:order_id>/delivery', endpoint_delivery.signature_delivery),
    path('rest/version/1/providers/',endpoint_providers.providers_query_result)
]
