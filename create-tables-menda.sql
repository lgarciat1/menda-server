CREATE DATABASE IF NOT EXISTS mendadb;

USE mendadb;

CREATE TABLE tUser (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  surname VARCHAR(100) NOT NULL,
  email VARCHAR(200) NOT NULL UNIQUE,
  business_name VARCHAR(100) NOT NULL,
  business_phone_country_code VARCHAR(4) NOT NULL,
  business_phone_number VARCHAR(11) NOT NULL,
  profile_type VARCHAR(20) NOT NULL,
  encrypted_password VARCHAR(100) NOT NULL,
  active_session_token CHAR(20)
);

CREATE TABLE tConstruction (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  building_name VARCHAR(100) NOT NULL,
  architect VARCHAR(100) NOT NULL,
  hirer VARCHAR(100) NOT NULL,
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  address VARCHAR(200) NOT NULL,
  latitude DECIMAL(8,6) NOT NULL,
  longitude DECIMAL(9,6) NOT NULL,
  author_id INTEGER NOT NULL,
  
  FOREIGN KEY (author_id) REFERENCES tUser(id)
);

CREATE TABLE tOrder (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  status VARCHAR(20) NOT NULL DEFAULT 'pending_pricing',
  date_created DATE NOT NULL DEFAULT CURDATE(),
  construction_id INTEGER NOT NULL,
  provider_id INTEGER NOT NULL,
  delivery_builder_signature BLOB,
  delivery_carrier_signature BLOB,
  
  FOREIGN KEY (construction_id) REFERENCES tConstruction(id),
  FOREIGN KEY (provider_id) REFERENCES tUser(id)
);

CREATE TABLE tOrderItem (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  order_id INTEGER NOT NULL,
  item_name VARCHAR(200) NOT NULL,
  quantity INTEGER NOT NULL,
  measurement_unit VARCHAR(2) NOT NULL,
  price INTEGER,
  accepted_quantity INTEGER,
  
  FOREIGN KEY (order_id) REFERENCES tOrder(id)
);
